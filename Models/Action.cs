﻿using UnityEngine;

namespace MltnTweakCore.Models
{
    [System.Serializable]
    public class Action
    {
        public string type;
        public object value;
        
        public void Perform(GameObject obj)
        {
            switch (type)
            {
                case "SetActive":
                    obj.SetActive(value as bool? ?? false);
                    break;
                case "SetParent":
                    Patcher.PendingParentalChanges.Add(obj, null);
                    break;
                case "RenderMaterial":
                    foreach (var componentsInChild in obj.GetComponentsInChildren<Renderer>())
                    {
                        componentsInChild.material = null;
                    }
                    break;
            }
        }
    }
}