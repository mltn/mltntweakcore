﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace MltnTweakCore.Models
{
    [System.Serializable]
    public class Task
    {
        public string type;
        public Processor[] processors;

        private void PerformRecursive(GameObject obj)
        {
            foreach (var processor in processors)
            {
                processor.Process(obj);
            }

            var childCount = obj.transform.childCount;
            for (var i = 0; i < childCount; i++)
            {
                var child = obj.transform.GetChild(i);
                PerformRecursive(child.gameObject);
            }
        }

        public void Perform()
        {
            Debug.Log("Performing task: " + type);
            switch (type)
            {
                case "RootObjectsRecursive":
                    var sceneRootObjects = SceneManager.GetActiveScene().GetRootGameObjects();
                    foreach (var rootObject in sceneRootObjects)
                    {
                        PerformRecursive(rootObject);
                    }
                    break;
                case "RootObjects":
                    var rootObjects = SceneManager.GetActiveScene().GetRootGameObjects();
                    foreach (var rootObject in rootObjects)
                    {
                        foreach (var processor in processors)
                        {
                            processor.Process(rootObject);
                        }
                    }
                    break;
            }
        }
    }
}