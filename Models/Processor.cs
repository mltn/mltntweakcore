﻿using UnityEngine;

namespace MltnTweakCore.Models
{
    [System.Serializable]
    public class Processor
    {
        public Condition condition;
        public Action[] actions;
        
        public void Process(GameObject obj)
        {
            Debug.Log("Processing " + obj.name + " as " + condition);
            var res = condition.Eval(obj);
            if (!res) return;
            
            foreach (var action in actions)
            {
                action.Perform(obj);
            }
        }
    }
}