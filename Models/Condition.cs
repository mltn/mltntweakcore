﻿using System.Linq;
using UnityEngine;

namespace MltnTweakCore.Models
{
    [System.Serializable]
    public class Condition
    {
        public string type;
        public string gate;
        public Condition[] values;
        public object value;
        public bool negate;

        public bool Eval(GameObject obj)
        {
            var result = EvalInternal(obj);
            if (negate) result = !result;
            return result;
        }
        
        private bool EvalInternal(GameObject obj)
        {
            switch (type)
            {
                case "Gate":
                    switch (gate)
                    {
                        case "AND":
                            return values.All(v => v.Eval(obj));
                        case "OR":
                            return values.Any(v => v.Eval(obj));
                    }
                    break;
                case "NameContains":
                    switch (value)
                    {
                        case string str:
                            return obj.name.Contains(str);
                        case string[] strings:
                        {
                            return strings.Any(str => obj.name.Contains(str));
                        }
                    }
                    break;
                case "NameEquals":
                    switch (value)
                    {
                        case string str:
                            return obj.name == str;
                        case string[] strings:
                            return strings.Any(str => obj.name == str);
                    }
                    break;
            }
            
            return false;
        }
    }
}