﻿using System.Collections.Generic;
using MltnTweakCore.Models;
using UnityEngine;

namespace MltnTweakCore
{
    public static class Patcher
    {
        public static Dictionary<GameObject, GameObject> PendingParentalChanges;

        public static Patch JsonPatchToPatchObject(string jsonPatch)
        {
            Debug.Log("Parsing patch: " + jsonPatch);
            var patchObject = Newtonsoft.Json.JsonConvert.DeserializeObject<Patch>(jsonPatch);
            return patchObject;
        }
        
        public static void ApplyWorldPatch(string jsonPatch)
        {
            ApplyWorldPatch(JsonPatchToPatchObject(jsonPatch));
        }
        
        public static void ApplyWorldPatch(Patch patch)
        {
            PendingParentalChanges = new Dictionary<GameObject, GameObject>();
            foreach (var patchTask in patch.tasks)
            {
                patchTask.Perform();
            }
            foreach (var pendingParentalChange in PendingParentalChanges)
            {
                pendingParentalChange.Key.transform.parent = pendingParentalChange.Value == null ? null : pendingParentalChange.Value.transform;
            }
        }
    }
}